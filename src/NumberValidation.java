import java.util.Scanner;

public class NumberValidation {
    private int registrationNumber;

    public void setRegistrationNumber() {
        System.out.println("Enter RegistrationNumber:- ");
        Scanner scannerobject=new Scanner(System.in);
        registrationNumber=scannerobject.nextInt();
    }

    public boolean checkNumber(){
        int odd=0;
        int even=0;
        int rem=0;
        while(registrationNumber>0){
            rem=registrationNumber%10;
            if(rem%2==0){
                even=even+rem;
            }
            else{
                odd=odd+rem;
            }
            registrationNumber=registrationNumber/10;
        }
        if(odd==even){
            return true;
        }
        else{
            return false;
        }
    }


}
